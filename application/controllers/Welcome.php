<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'libraries/aws/Aws-autoloader.php';
require APPPATH.'libraries/aws/Config.php';
use Aws\S3\S3Client;

class Welcome extends CI_Controller {

    const NAME_LENGTH = 30;

    public function index(){

        $this->load->view('welcome');
    }

    /**
     * uploadImage method
     * Upload selected images to server
     *
     * @public
     * @return {String}
     */
    public function uploadImage(){

        $s3Client = $this->_setClient();

        $total = count($_FILES['files']['name']);
        $tmpFilePath = $_FILES['files']['tmp_name'][0];

        if ($tmpFilePath != ""){
            $type=$_FILES['files']['type'][0];

            $fileName = $_FILES['files']['name'][0];
            $fileExtension = substr($fileName, strrpos($fileName, '.'));
            $keyname = $this->_generateRandomString(self::NAME_LENGTH).$fileExtension;

            $result = $s3Client->putObject([
                'Bucket'       => 'sliderzz',                  
                'Key'          => $keyname,
                'SourceFile'   => $tmpFilePath,
                'ContentType'  => $type,
                'ACL'          => 'public-read'
            ]);
        }

        echo json_encode(array('name' => $keyname));
    }

    /**
     * getBucket method
     * Get all images from bucket
     *
     * @public
     * @return {String}
     */
    public function getBucket(){

        $s3Client = $this->_setClient();

        $result = $s3Client->listObjects(array('Bucket' => 'sliderzz'));
        $bucketkeys = array();
        foreach ($result['Contents'] as $object) {
            $aux_uploaded = strtotime($object['LastModified']);
            $bucketkeys[] = array('time' => $aux_uploaded, 'key' => $object['Key']);
        }

        usort($bucketkeys, array('Welcome','_sortByOrder'));

        echo json_encode($bucketkeys);
    }

    /**
     * _setClient method
     * Set client connection
     *
     * @private
     * @return {Object}
     */
    private function _setClient() {
        $s3 = new Config;
        return $s3->_initClient();
    }

    /**
     * _sortByOrder method
     * Order an array Desc
     *
     * @private static
     * @return {Array}
     */
    private static function _sortByOrder($a, $b) {
        return $b['time'] - $a['time'];
    }

    /**
     * _generateRandomString method
     * Return a random string by length
     *
     * @private
     * @return {String}
     */
    private function _generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
