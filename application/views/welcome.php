<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sliderzz!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link href="assets/js/fileuploader/css/jquery.fileuploader.css" media="all" rel="stylesheet">
    <link href="assets/js/fileuploader/css/jquery.fileuploader-theme-dragdrop.css" media="all" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/js/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
  </head>
  <div class="back"></div>
  <body>
    <div class="row col-md-12">

        <div class="container text-center">

          <section class="panel panel-default">
            <header class="panel-heading f-bold">SLIDERZZ</header>
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#home">Upload Images</a></li>
              <li id="gallerytab"><a data-toggle="tab" href="#gallery">View Images</a></li>
            </ul>
            

            <div class="tab-content">

                <div id="home" class="tab-pane fade in active">
                    <div class="row m-n">
                     <div class="col-lg-12 text-center" style="padding:20px 40px;">
                        <div id="uploader-wrapper" class="col-md-12">
                            <div class="row text-center push-10-t">
                                <input type="file" name="files">
                            </div>
                        </div>
                      </div>
                    </div>
                </div>

                <div id="gallery" class="tab-pane fade in">
                    <div class="row m-n">
                     <div class="col-lg-12 text-center">
                        <div class='alert alert-info col-md-12 text-center'>Loading content...</div>
                      </div>
                    </div>
                </div>
            </div>
          </section>
        </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/fileuploader/js/jquery.fileuploader.js" type="text/javascript"></script>
    <script src="assets/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5" type="text/javascript"></script>
    <script src="assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7" type="text/javascript"></script>
    <script src="assets/js/fileuploader/js/custom.js" type="text/javascript"></script>
  </body>
</html>