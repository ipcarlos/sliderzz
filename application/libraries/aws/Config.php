<?php

class Config{
	
	/**
     * _initClient method
     * Init connection with bucket
     *
     * @public
     * @return {object}
     */
    public function _initClient(){
        date_default_timezone_set('Europe/Madrid');

        $sdk = new Aws\Sdk([
            'region'   => 'your-region',
            'version'  => 'latest',
            'credentials' => [
                'key'    => 'your-key',
                'secret' => 'your-secret'
            ]
        ]);

        return $sdk->createS3();
    }
}