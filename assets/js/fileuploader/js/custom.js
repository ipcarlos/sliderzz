$(document).ready(function() {
	
	var aux_bucket_url = 'https://s3.eu-west-3.amazonaws.com/sliderzz/';

	$('input[name="files"]').fileuploader({
        limit: 5,
        maxSize: 5,
        fileMaxSize: 5,
        extensions: ['jpg', 'jpeg', 'gif', 'png'],
        changeInput: '<div class="fileuploader-input">' +
					      '<div class="fileuploader-input-inner">' +
						      '<img src="'+$('#htmlbaseurl').text()+'assets/js/fileuploader/images/fileuploader-dragdrop-icon.png">' +
							  '<h3 class="fileuploader-input-caption"><span>Drag and Drop Images Here</span></h3>' +
							  '<p>or</p>' +
							  '<div class="fileuploader-input-button"><span>Browse Images</span></div>' +
						  '</div>' +
					  '</div>',
        theme: 'dragdrop',
		upload: {
            url: 'welcome/uploadImage',//php/ajax_upload_file.php
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            beforeSend: null,
            onSuccess: function(result, item) {
                console.log(item.name);
                var aux_link = aux_bucket_url + JSON.parse(result).name;
                $('.fileuploader-item.upload-successful .column-title div[data-title="'+item.name+'"]').next().append('<a href="'+aux_link+'" class="aws-link" target="_blank">'+aux_link+'</a>');
            },
            onError: function(item) {
				var progressBar = item.html.find('.progress-bar2');
				
				if(progressBar.length > 0) {
					progressBar.find('span').html(0 + "%");
                    progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
					item.html.find('.progress-bar2').fadeOut(400);
				}
                
                item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                    '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                ) : null;
            },
            onProgress: function(data, item) {
                var progressBar = item.html.find('.progress-bar2');
				
                if(progressBar.length > 0) {
                    progressBar.show();
                    progressBar.find('span').html(data.percentage + "%");
                    progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                }
            },
            onComplete: null,
        },
		onRemove: function(item) {
			$.post('welcome/removeImage', {
				file: item.name
			});
		},
		captions: {
            feedback: 'Drag and Drop Images Here',
            feedback2: 'Drag and Drop Images Here',
            drop: 'Drag and Drop Images Here'
        },
	});

    $('#gallerytab').on('click', function(e){
        $.ajax({
            url: 'welcome/getBucket',
            type: 'POST',
            dataType: 'json',
            success: function(response){
                var aux_append = '';

                $.each(response, function(k,v){
                    aux_append += '<div class="bucket-img"><a class="fancybox-thumbs" data-fancybox-group="thumb" href="'+aux_bucket_url+v.key+'"><img src="'+aux_bucket_url+v.key+'" /></a></div>';
                });

                $("#gallery .col-lg-12").html(aux_append);

            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $("#gallery .col-lg-12").html("<div class='alert alert-warning col-md-12 text-center'>An error occurred when loading content.</div>");
            }
        });
    });

    $('.fancybox').fancybox();

    $('.fancybox-thumbs').fancybox({
        openEffect : 'elastic',
        closeEffect : 'elastic',
        prevEffect : 'none',
        nextEffect : 'none',

        closeBtn  : true,
        arrows    : true,
        nextClick : true,

        helpers : {
            thumbs : {
                width  : 100,
                height : 100
            }
        }
    });
});